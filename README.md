# BeWine

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.4.

## Development server

Before run `ng serve` you need to install nodeJS and install all required npm packages by running `npm install` from the command line in the project root folder.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production link

https://be-wine.herokuapp.com/drink/list/wine

## Considerations
I used the Angular quickstart project as a base for the application, it's written in TypeScript and uses Angular Material to some style.

The path sctructure is as bellow:

```
be-wine
│   e2e
│   node_modules    
│   
└───src
    │   
    └───app
        │   app.component.ts|html|ts|scss|spec.ts
        |   app.module.ts
        |   app-routing.module.ts
        |   in-memory-data.service.ts|spec.ts
        │   
        └───drinks
        |   │   
        |   └───add-drink
        |   |   └───add-drink.component.ts|html|scss|spec.ts
        |   │   
        |   └───drink-details
        |   |   └───drink-details.component.ts|html|scss|spec.ts
        |   │   
        |   └───drink-list
        |   |   └───drink-list.component.ts|html|scss|spec.ts
        |   │   
        |   └───shared
        |       │   
        |       └───models
        |       |   |   beer.model.ts
        |       |   |   wine.model.ts
        |       |   └───drinks.model.ts   
        |       │   
        |       └───services
        |           └───drinks.services.ts|spec.ts 
        │   
        └───shared
            └───nav-menu
                └───nav-menu.component.ts|html|scss|spec.ts
        

```



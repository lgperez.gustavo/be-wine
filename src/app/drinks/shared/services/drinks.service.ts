import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WineModel } from '../Models/wine.model';
import { HttpClient } from '@angular/common/http';
import { BeerModel } from '../Models/beer.model';

@Injectable({
  providedIn: 'root'
})
export class DrinksService {

  private winesUrl = 'api/wines';
  private beersUrl = 'api/beers';
  public drinkType: string;

  constructor(private http: HttpClient) { }

  public getWines(): Observable<WineModel[]> {
    return this.http.get<WineModel[]>(this.winesUrl);
  }

  public getBeers(): Observable<BeerModel[]> {
    return this.http.get<BeerModel[]>(this.beersUrl);
  }

  public getWineDetail(drinkId: string): Observable<WineModel> {
    const url = `${this.winesUrl}/${drinkId}`;
    return this.http.get<WineModel>(url);
  }

  public getBeerDetail(drinkId: string): Observable<BeerModel> {
    const url = `${this.beersUrl}/${drinkId}`;
    return this.http.get<BeerModel>(url);
  }

  public addWine(wine: WineModel): Observable<WineModel> {
    return this.http.post<WineModel>(this.winesUrl, wine);
  }

  public addBeer(beer: BeerModel): Observable<WineModel> {
    return this.http.post<WineModel>(this.beersUrl, beer);
  }
}

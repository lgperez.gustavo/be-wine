export interface DrinksModel {
    id: number;
    name: string;
    year: number;
    price: number;
}

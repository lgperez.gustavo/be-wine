import { DrinksModel } from './drinks.model';

export interface WineModel extends DrinksModel {
    vineyard: string;
}

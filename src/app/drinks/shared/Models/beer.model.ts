import { DrinksModel } from './drinks.model';

export interface BeerModel extends DrinksModel {
    brand: string;
    type: string;
}

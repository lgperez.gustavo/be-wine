import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DrinksService } from '../shared/services/drinks.service';
import { WineModel } from '../shared/Models/wine.model';
import { DrinksModel } from '../shared/Models/drinks.model';

@Component({
  selector: 'app-drink-detail',
  templateUrl: './drink-detail.component.html',
  styleUrls: ['./drink-detail.component.scss']
})
export class DrinkDetailComponent implements OnInit {

  public drink: DrinksModel;

  private drinkType: string;

  constructor(
    private activeRoute: ActivatedRoute,
    private drinksService: DrinksService,
    private location: Location
    ) { }

  ngOnInit() {
    const drinkId = this.activeRoute.snapshot.paramMap.get('drinkId');
    this.drinkType = this.activeRoute.snapshot.paramMap.get('drinkType');

    switch (this.drinkType) {
      case 'wine':
        this.drinksService.getWineDetail(drinkId).subscribe(
          wine => {
            this.drink = wine;
          }
        );
        break;
      case 'beer':
        this.drinksService.getBeerDetail(drinkId).subscribe(
          beer => {
            this.drink = beer;
          }
        );
        break;
      default:
        break;
    }
  }

  public onBackClick(): void {
    this.location.back();
  }

}

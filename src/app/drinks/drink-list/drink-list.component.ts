import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DrinksService } from '../shared/services/drinks.service';
import { DrinksModel } from '../shared/Models/drinks.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-drink-list',
  templateUrl: './drink-list.component.html',
  styleUrls: ['./drink-list.component.scss']
})
export class DrinkListComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  public displayedColumns: string[] = ['id', 'name', 'year', 'price', 'detail'];

  private tableDataSource: DrinksModel[];
  
  private drinkType: string;

  public dataSource = new MatTableDataSource(this.tableDataSource);

  constructor(
    private activeRoute: ActivatedRoute,
    private drinksService: DrinksService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.initializeTable();
  }

  private initializeTable() {
    this.activeRoute.paramMap.subscribe(route => {
      this.drinkType = route.get('drinkType');
      switch (this.drinkType) {
        case 'wine':
          this.drinksService.getWines().subscribe(data => {
            this.updateDataTable(data);
          });
          break;
        case 'beer':
          this.drinksService.getBeers().subscribe(data => {
            this.updateDataTable(data);
          });
          break;
        default:
          break;
      }
    });
  }

  private updateDataTable(data: DrinksModel[]) {
    this.tableDataSource = data;
    this.dataSource = new MatTableDataSource(this.tableDataSource);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public drinkDetail(drinkId: number, drinkType: string): void {
    this.router.navigate(['drink/detail', drinkType, drinkId]);
  }

  public onDrinkAdded(drink): void {
    this.tableDataSource.push(drink);

    this.dataSource.data = this.tableDataSource;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DrinksService } from '../shared/services/drinks.service';
import { DrinksModel } from '../shared/Models/drinks.model';

@Component({
  selector: 'app-add-drink',
  templateUrl: './add-drink.component.html',
  styleUrls: ['./add-drink.component.scss']
})
export class AddDrinkComponent implements OnInit {

  public isBeer: boolean;

  public drinkForm: FormGroup;
  public drinkType: string;

  @Output() public drinkAdded = new EventEmitter<DrinksModel>();

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private drinksService: DrinksService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      route => {
        this.drinkType = route.get('drinkType');

        switch (this.drinkType) {
          case 'wine':
            this.createWineForm();
            this.isBeer = false;
            break;

          case 'beer':
            this.createBeerForm();
            this.isBeer = true;
            break;
          default:
            break;
        }
      }
    );
  }

  private createBeerForm(): void {
    this.drinkForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      brand: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      type: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      year: [null, Validators.compose([Validators.required, Validators.pattern(/^(199\d|200\d|201\d)$/)])],
      price: [null, Validators.required]
    });
  }

  private createWineForm(): void {
    this.drinkForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      vineyard: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      year: [null, Validators.compose([Validators.required, Validators.pattern(/^(199\d|200\d|201\d)$/)])],
      price: [null, Validators.required]
    });
  }

  public addDrink(drink): void {
    if (this.isBeer) {
      this.drinksService.addBeer(drink).subscribe(
        newBeer => {
          this.drinkAdded.emit(newBeer);
        }
      );
    } else {
      this.drinksService.addWine(drink).subscribe(
        newWine => {
          this.drinkAdded.emit(newWine);
        }
      );
    }
    this.drinkForm.reset();
  }
}

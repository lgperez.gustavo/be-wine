import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DrinkListComponent } from './drinks/drink-list/drink-list.component';
import { DrinkDetailComponent } from './drinks/drink-detail/drink-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/drink/list/wine',
    pathMatch: 'full'
  },
  {
    path: 'drink/list/:drinkType',
    component: DrinkListComponent,
  },
  {
    path: 'drink/detail/:drinkType/:drinkId',
    component: DrinkDetailComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
